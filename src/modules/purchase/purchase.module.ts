import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { HomeComponent } from "../../components/home/home.component";
import { SiteSearchComponent } from "../../components/site-search/site-search.component";

import { PaymentComponent } from "../../components/payment/payment.component";
import { ConfirmationComponent } from "../../components/confirmation/confirmation.component";
import { PurchaseRouterModule } from "./purchase.module.routing";

@NgModule({
  imports: [SharedModule, PurchaseRouterModule],
  declarations: [
    PaymentComponent,
    ConfirmationComponent,
    SiteSearchComponent,
    HomeComponent
  ]
})
export class PurchaseModule {
  constructor() {}
}
