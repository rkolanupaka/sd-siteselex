import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AccountManagementRouterModule } from './account-management.module.routing';

import { AccountManagementComponent } from '../../components/account-management/account-management.component';

import { MySiteRequestsComponent } from '../../components/my-site-requests/my-site-requests.component';
import { WaitListSitesComponent } from "../../components/wait-list-sites/wait-list-sites.component";
import { MyInvoiceComponent } from "../../components/my-invoice/my-invoice.component";
import {MatTableModule} from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';

@NgModule({
    imports: [SharedModule, AccountManagementRouterModule, MatTableModule, CdkTableModule],
    declarations: [AccountManagementComponent, MySiteRequestsComponent, WaitListSitesComponent, MyInvoiceComponent]
})

export class AccountManagementModule {
    constructor (){}
}