import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HeaderNewComponent } from '../../components/header-new/header-new.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { CommonRequestNow } from '../../components/common-request-now/common-request-now.component';
import { CustomPopupComponent } from '../../components/custom-popup/custom-popup.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CookieModule } from 'ngx-cookie';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragNDropDirective } from '../../common/directives/dragndrop.directive';
import { OrderFilterPipe, CityFilterPipe, searchFilterPipe } from '../../common/pipes/filters.pipe';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import {MatListModule} from '@angular/material/list';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTabsModule} from '@angular/material/tabs';
// import {MatTableModule} from '@angular/material/table';
// import { CdkTableModule } from '@angular/cdk/table';
import { AgmCoreModule } from '@agm/core';


@NgModule({
    imports: [ 
        CommonModule, 
        FormsModule, 
        RouterModule,
        NgxPaginationModule,
        InfiniteScrollModule,
        // Ng2AutoCompleteModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        // CdkTableModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        MatTabsModule,
        // MatTableModule,
        ReactiveFormsModule,
        CookieModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAKIRNlT44mTTMRaL0Wm3npkMf-Deyasjw'
        })
    ],
    declarations: [ 
        HeaderNewComponent,
        CommonRequestNow,
        CustomPopupComponent, 
        FooterComponent,
        DragNDropDirective,
        OrderFilterPipe, 
        CityFilterPipe,  
        searchFilterPipe
    ],
    exports: [
        AgmCoreModule,
        HeaderNewComponent,
        CommonRequestNow,
        FormsModule, 
        CommonModule, 
        CustomPopupComponent, 
        FooterComponent,
        NgxPaginationModule,
        InfiniteScrollModule,
        DragNDropDirective,
        OrderFilterPipe, 
        CityFilterPipe, 
        searchFilterPipe,
        MatAutocompleteModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        MatTabsModule,
        CookieModule
    ]
})

export class SharedModule {}