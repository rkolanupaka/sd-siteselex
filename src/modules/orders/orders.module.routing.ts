import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderHistoryComponent } from '../../components/order-history/order-history.component';
import { SwapSiteComponent } from '../../components/swap-site/swap-site.component';
import { SiteSwapConfirmationComponent } from '../../components/site-swap-confirmation/site-swap-confirmation.component';

const routes:Routes = [
    {
        path: '',
        component: OrderHistoryComponent
    },
    {
        path: 'swap-site',
        component: SwapSiteComponent
    },
    {
        path: 'swap-site-confirmation',
        component: SiteSwapConfirmationComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class OrdersRouterModule {

}