import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "orderFilter"  // user in order history page to filter orders based on order no. or site id
})
export class OrderFilterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2): any {
    let filter1 = arg1;
    let filter2 = arg2;
    if (filter2.length == 0) {
      return value;
    } else {
      filter2.forEach(element => {
        if (element.name == "Order Number") {
          value = value.filter(orderItem => orderItem.increment_id.toLocaleLowerCase().indexOf(element.value) != -1);
        }
        if (element.name == "Site ID") {
          let tempVal = element.value;
          value = value.filter(orderItem => {
            let tempCheck: boolean = false;
            orderItem.items.forEach(element => {
              if (element.sku.toLocaleLowerCase().indexOf(tempVal) != -1) {
                tempCheck = true;
              }
            });
            return tempCheck;
          });
        }
      });
      return value;
    }
  }
}

@Pipe({
  name: "cityFilter"  //used in admin site management page to filter cities based on name and status
})
export class CityFilterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2): any {
    let filter = "";
    let filter2 = arg2;
    console.log(filter);
    console.log(filter2);
    if (typeof arg1 == "object") {
      filter = arg1.city.toLocaleLowerCase();
    } else {
      filter = arg1.toLocaleLowerCase();
    }
    if (filter == "" && filter2 == "") {
      return value;
    } else {
      value = value.filter(cityItem => cityItem.city.toLocaleLowerCase().indexOf(filter) != -1);
      return value.filter(cityItem => cityItem.cityStatus.toLocaleLowerCase().indexOf(filter2) != -1);
    }
  }
}


@Pipe({
  name: "searchFilter"  //used in admin site management page to filter sites by site id
})
export class searchFilterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2: any): any {
    let filter = arg1.toLocaleLowerCase();
    let filter2 = arg2.toLocaleLowerCase();
    if (filter == "" &&filter2 == "") {
      return value;
    } else {
      value = value.filter(item => item.sku.toLocaleLowerCase().indexOf(filter) != -1);
      return value.filter(item => item.stat.toLocaleLowerCase().indexOf(filter2) != -1);
    }
  }
}
