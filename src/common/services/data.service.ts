import { Injectable, Inject } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

import { environment } from '../../environments/environment';

@Injectable()
export class DataService {
  baseHost:string;
  baseUrl:string;

  constructor(public http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseHost = 'http://'+ this.document.location.hostname;  //getting host name as per environment app is running on
    // this.baseHost = 'http://18.220.231.143';  //temporary setting if local magento setup not done 
    this.baseUrl = environment.baseUrl;  //getting common base url after host from environment files as in local setup it has magento and in prod server it has magento2
  }

  //http GET call
  fetchData(url, headerOpt?){
    let completeUrl = this.baseHost + this.baseUrl + url;
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.get(completeUrl, httpOptions);
  }

  //http local json file calls
  fetchLocalData(url, headerOpt?){
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.get(url, httpOptions);
  }

  //http GET call for response type text. Ex reports API
  fetchPlainData(url, headerOpt?) {
    let completeUrl = this.baseHost + this.baseUrl + url;
    return this.http.get(completeUrl, {responseType: 'text'});
  }

  // http POST call
  fetchPostData(url, body, headerOpt?){
    let completeUrl = this.baseHost + this.baseUrl + url;
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.post(completeUrl,body, httpOptions);
  }

  //http PUT call
  fetchPutData(url, body, headerOpt?){
    let completeUrl = this.baseHost + this.baseUrl + url;    
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.put(completeUrl,body, httpOptions);
  }

  // http DELETE call
  fetchDeleteData(url, headerOpt?){
    let completeUrl = this.baseHost + this.baseUrl + url;
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.delete(completeUrl, httpOptions);
  }

  // most probably not using this now anywhere else
  getHostName() {
    return this.baseHost;
  }
}
