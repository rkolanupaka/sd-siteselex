import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(
        private _cookieService:CookieService
    ) {}

    canActivate(): boolean {
        if(this._cookieService.get('5barsToken')) { //5barsToken is set in cookie as soon as user logs in. so if it exists we allow all other routs else not.
            return true;
        }
        return false;
    }
}
