import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {
    userData:any;

    constructor() {}

    canActivate(): boolean {
        //makes use of fivebarsUserData attribute in localstorage which has logged in user info to check if it is admin or not
        this.userData = JSON.parse(localStorage.getItem('fivebarsUserData'));
        let isAdmin = false;
        if(this.userData && this.userData.custom_attributes) {
            (this.userData.custom_attributes).forEach((element) => {
                if(element.attribute_code == 'is_admin') {
                    if(element.value == "1") {
                        isAdmin = true;
                    }
                }
            });
        }
        return isAdmin;
    }
}
