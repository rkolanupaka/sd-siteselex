import { Directive, HostListener, HostBinding, EventEmitter, Output } from "@angular/core";

@Directive({
    selector : '[fiveBarsDnd]'
})

export class DragNDropDirective {
    @Output() public fileChangedEmitter : EventEmitter<FileList> = new EventEmitter();

    constructor() { }

    @HostListener('dragover', ['$event']) ondragover(evt){
        evt.preventDefault();
        evt.stopPropagation();
    }

    @HostListener('dragleave', ['$event']) public onDragLeave(evt){
        evt.preventDefault();
        evt.stopPropagation();
    }

    @HostListener('drop', ['$event']) public onDrop(evt){
        evt.preventDefault();
        evt.stopPropagation();
        let files = evt.dataTransfer.files;
        if(files.length > 0 && files[0].type == 'application/vnd.ms-excel'){
          this.fileChangedEmitter.emit(files);   //calls onFileChangeInDirective function in site-search.component.ts
        }
    }
}