import { Component, OnInit, ViewChild} from '@angular/core';
import { CookieService } from 'ngx-cookie';
import {DataService} from '../../common/services/data.service';

@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.scss']
})
export class AccountManagementComponent implements OnInit {
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  showLoader:boolean = false;
  fiveBarsToken:string;
  loggedUserData:any;
  invoiceTabActive:number = 0;

  constructor(
    private _cookieService:CookieService,
    private dataservice: DataService
  ) {
    this.fiveBarsToken = this._cookieService.get('5barsToken');
  }

  ngOnInit() {
  }

  receivedUserData($event) {
    console.log($event);
    this.loggedUserData = $event;
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  selectedTabChange(event) {
    if(event.index == 2) {
      this.loadPoAndPcDetails();
    }
    if(event.index == 3) {
      this.invoiceTabActive = this.loggedUserData.id;
    }
  }


  // load po numbers and project contacts on page load starts
  loadPoAndPcDetails(){
    let url = "customer/setup/"+this.loggedUserData.id;
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data:any)=>{
      this.showLoader = false;
      this.ponumberdata = data.po_number.items;
    }, (err)=> {
      this.showLoader = false;      
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "Something went wrong while loading your PO numbers and Project Contacts.";
      }
      this.showPopUpTrue = true;
    })
  }

  // delete po number method starts
  deletePo(itemId, index) {
    let url = 'customer/ponumber/'+itemId;
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchDeleteData(url, headerDict).subscribe((data)=>{
      this.showLoader = false;
      this.ponumberdata.splice(index, 1);
    }, (error)=>{
      this.showLoader = false;
      this.showPopUpMessage = "There was some issue deleting Purchase Order. Kindly try again.";
      this.showPopUpTrue = true;
    });
  }

  addNewPoTrue:boolean = false;
  poAddFlag:boolean = true;
  newPoNumber:string = "";
  newPoComment:string = "";
  editItem:any;
  editIndex:string;
  @ViewChild('newPoNumberInp') newPoNumberInp;
  addPo() {
    this.addNewPoTrue=true;
    this.poAddFlag=true;
    this.newPoNumber = "";
    this.newPoComment = "";
    this.editItem = "";
    this.editIndex = "";
    
    setTimeout(()=> {
      this.newPoNumberInp.nativeElement.focus();
    }, 1);
  }

  ponumberdata:any = [];
  // save new or edit po number method starts
  savePoNo(poForm) {
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    let url = 'customer/ponumber';
    let data:any = {
      "customerId" : this.loggedUserData.id,
      "ponumber" : poForm.value.newPoNumber,
      "comment" : poForm.value.newPoComment
    }
    this.showLoader = true;
    if(this.poAddFlag == true) {
      this.dataservice.fetchPostData(url, data, headerDict).subscribe((data)=>{
        let newPoId = data;
        this.ponumberdata.push({
          "po_id" : newPoId,
          "po_number" : poForm.value.newPoNumber,
          "comment" : poForm.value.newPoComment
        })
        this.showLoader = false;
        this.addNewPoTrue = false;
      }, (error)=>{
        this.poSaveErrorFunc(error);
      });
    } else {
      data.poId = this.editItem.po_id;
      this.dataservice.fetchPutData(url,data, headerDict).subscribe((data)=>{
        this.showLoader = false;
        this.editItem.po_number = poForm.value.newPoNumber;
        this.editItem.comment = poForm.value.newPoComment;
        this.addNewPoTrue = false;
      }, (error)=>{
        this.poSaveErrorFunc(error);
      });
    }
  }

  // edit po number method starts
  editPo(item, index) {
    this.addNewPoTrue = true;
    this.poAddFlag = false;
    this.newPoNumber = item.po_number;
    this.newPoComment = item.comment;
    this.editItem = item;
    this.editIndex = index;
    setTimeout(()=> {
      this.newPoNumberInp.nativeElement.focus();
    }, 1);
  }

  poSaveErrorFunc(err) {
    try {
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "There was an error. Please try again.";
      }
    } 
    catch (err) {
      this.showPopUpMessage = "There was an error. Please try again.";
    }
    this.showPopUpTrue = true;
    this.showPopUpTrue = true;
    this.showLoader = false;
    this.addNewPoTrue = false;
  }

}
