import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

import { DataService } from "../../common/services/data.service";
import { CookieService } from "ngx-cookie";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  loggedUserData: any;  
  fiveBarsToken: string;
  showLoader: boolean = false;
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  featuredCities:any = [];
  citySelected:any;

  constructor(
    private router: Router,
    private dataservice: DataService,
    private _cookieService: CookieService
  ) {
    this.fiveBarsToken = this._cookieService.get("5barsToken");
  }

  // gets called from header after user details api call is done
  receivedUserData($event) {
    this.loggedUserData = $event;
    this.loadFeaturedSites();
  }

  loadFeaturedSites() {
    let url = "custom/featuredsites/" + this.loggedUserData.id;
    let headerDect = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe(
      data => {
        this.showLoader = false;
        console.log(data);
        this.featuredCities = data;
      },
      err => {
        this.showLoader = false;
        if (err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage =
            "Something went wrong while loading featured cities.";
        }
        this.showPopUpTrue = true;
      }
    );
  }

  // in search page we pre select state and city if it is present in local storage so on click of featured site setting state/city
  navigateToReserve(state, city) {
    if(state && city) {
      localStorage.setItem(
        "preStateCity",
        state + "***" + city
      );
      this.router.navigate(['site/site-search'], { queryParams: {type:"findSiteByInput"}});
    }
  }

  navigateToReserveMobile() {
    if(this.citySelected.state) {
      this.navigateToReserve(this.citySelected.state, this.citySelected.city);
    }
  }
}
