import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {DataService} from '../../common/services/data.service';

@Component({
  selector: 'app-my-invoice',
  templateUrl: './my-invoice.component.html',
  styleUrls: ['./my-invoice.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MyInvoiceComponent implements OnInit, OnChanges {
  @Input() showInvoiceTab: number;
  showLoader:boolean = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;

  displayedColumns: string[] = ['InvoiceNumber', 'SkuId', 'CreatedAt', 'Qty', 'Price'];
  dataSource:Array<invoiceObj> = [];  

  constructor(
    private dataservice: DataService
  ) { }

  // we load invoice only when invoice tab is clicked in account management page
  ngOnChanges() {
    if(this.showInvoiceTab > 0) {
      this.loadInvoices();
    }
  }

  ngOnInit() {}

  // load invoices
  loadInvoices(){
    let url = "custom/invoices/"+this.showInvoiceTab;
    let headerDect = {
      "Content-Type": "application/json"
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data:any)=>{
      this.showLoader = false;
      console.log(data);
      if(data.length > 0) {
        data.forEach(element => {
          element.items.forEach(element2 => {
            let date = new Date(element.created_at);
            let dateString = date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear();
            this.dataSource.push({
              'InvoiceNumber': element.increment_id,
              'SkuId': element2.name,
              'CreatedAt': dateString,
              'Qty': element2.qty,
              'Price': element2.price
            })
          });
        });
      }
    }, (err)=> {
      this.showLoader = false;      
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "Something went wrong while loading your PO numbers and Project Contacts.";
      }
      this.showPopUpTrue = true;
    })
  }


}

export interface invoiceObj {
  'InvoiceNumber': string;
  'SkuId': string;
  'CreatedAt': string,
  'Qty': number;
  'Price': number;
}