import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  showLoader:boolean = false;
  showPopUpMessage:string;  
  showPopUpTrue:boolean = false;
  resetEmail = '';
  validateEmailFlag = false;
  resetOption = '';
  emailValidateError:string = '';
  sendEmailLinkError:string = '';
  invalidUserCount:number = 0;
  captchaVal;
  
  constructor(
    private router:Router,
    private dataservice:DataService
  ) { }
  
  ngOnInit() { }

  // to close the custom alert popup
  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  navigateToLogin() {
    this.router.navigate(['/login']);
  }
  
  // callback function for recaptcha resolve
  resolved(event) {
    console.log(event);
    this.captchaVal = event;
    this.emailValidateError = '';
  }

  // validates email address from first screen
  validateEmail() {
    console.log(this.resetEmail);
    if(this.invalidUserCount<2 || this.captchaVal) {
      this.emailValidateError = '';
      let headerDict = {
        'Content-Type': 'application/json'
      }
      this.showLoader = true;      
      let url = 'customers/password/validate/'+ this.resetEmail;
      this.dataservice.fetchData(url, headerDict).subscribe((data: any)=>{
        if(data == true) {
          this.showLoader = false;
          this.validateEmailFlag = true;
        } else {
          this.emailValidateError = "Not a registered customer or something else went wrong. Please try again";
        }
      }, (err)=>{
        this.invalidUserCount++;
        this.showLoader = false;
        this.validateEmailFlag = false;
        if (err.message) {
          this.emailValidateError = err.message;
        } else {
          this.emailValidateError = "something went wrong. Please login again";
        }
      });
    } else {
      this.emailValidateError = "Please solve the captcha to continue.";
    }
  }

  resetOptionSelected(restOption) {
    this.sendEmailLinkError = '';
    if(restOption === 'email') {
      this.sendEmailLink();
    } else {
      this.getQuestions();
      this.resetOption = 'online';
    }
  }

  securitySet1:any;
  securitySet2:any;
  securitySet3:any;

  // makes a api call to get question for screen 3
  getQuestions() {
    let headerDict = {
      'Content-Type': 'application/json'
    }
    this.showLoader = true;
    let url = 'ProfileManagement/questionnaire/questions/'+this.resetEmail;
    this.dataservice.fetchData(url, headerDict).subscribe((data: any)=>{
      this.showLoader = false;
      this.securitySet1 = data.questions.set1;
      this.securitySet2 = data.questions.set2;
      this.securitySet3 = data.questions.set3;
      console.log(data);
    }, (err)=>{
      this.showLoader = false;
      if (err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "something went wrong while retrieving security questions. Please try again";
      }
      this.resetOption = '';
      this.showPopUpTrue = true;
    });
  }

  // makes an api call to to send email link if user select send email option in screen 2
  sendEmailLink() {
    let headerDict = {
      "Content-Type": "application/json"
    }
    let userData = {
      "email": this.resetEmail,
      "template": "email_reset",
      "websiteId": 1
    }
    let url = 'customers/password';
    this.showLoader = true;
    this.dataservice.fetchPutData(url, userData, headerDict).subscribe((data)=>{
      if(data) {
        this.resetOption = 'email';
      } else {
        this.sendEmailLinkError = "something went wrong. Please login again";
      }
      this.showLoader = false;
    }, (err)=>{
      this.showLoader = false;
      if (err.message) {
        this.sendEmailLinkError = err.message;
      } else {
        this.sendEmailLinkError = "something went wrong. Please login again";
      }
    })
    
  }


  // validates and submits the form from screen 3 and navigates to create password page on success
  validateFinalForm(finalresetForm) {
    let headerDict = {
      'Content-Type': 'application/json'
    }
    let url = 'ProfileManagement/questionnaire/validate';
    let data = {
      "fName": finalresetForm.firstName,
      "lName": finalresetForm.lastName,
      "emailId": this.resetEmail,
      "postCode": finalresetForm.zip,
      "questions": [{
          "id": finalresetForm.securityQues_1,
          "setid": 1,
          "answer": finalresetForm.answer_1
        },
        {
          "id": finalresetForm.securityQues_2,
          "setid": 2,
          "answer": finalresetForm.answer_2
        },
        {
          "id": finalresetForm.securityQues_3,
          "setid": 3,
          "answer": finalresetForm.answer_3
        }
      ]
    }

    this.showLoader = true;
    this.dataservice.fetchPostData(url, data, headerDict).subscribe((data: any)=>{
      this.showLoader = false;
      if(data.valid) {
        let resetTokenLink = '/resetpassword/'+data.customer_id+'/'+data.token;
        this.router.navigate([resetTokenLink]);
      } else {
        this.showPopUpMessage = "something went wrong. Please try again";
        this.showPopUpTrue = true;
      }
    }, (err)=>{
      this.showLoader = false;
      if (err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "something went wrong. Please try again";
      }
      this.showPopUpTrue = true;
    });
  }

}
