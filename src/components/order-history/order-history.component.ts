import { Component, HostListener } from '@angular/core';

import { CookieService } from 'ngx-cookie';
import { DataService } from '../../common/services/data.service';
import { Router } from "@angular/router";
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss'],
})
export class OrderHistoryComponent {
  
  fiveBarsToken:string;
  filterValueObj:any = [];
  hostValue:string;
  itemsPerPage:number = 5;
  infiniteScrollCount:number = 10;
  showLoader:boolean = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  orderData:any = [];
  ordersFetched:boolean = false;
  parseInt = parseInt;
  pageNumber: number = 1;
  searchByType:string = "Order Number";
  showingTotal:number = 0;
  filterValue:string = "";
  searchValue:string = "";
  selectedDrawing:any = [];
  drawingImgSrc:string = '';
  imageEntries:any = [];
  baseImgUrl:string;

  constructor(
    private dataservice: DataService, 
    private _cookieService:CookieService,
    private router: Router
  ) {
    this.fiveBarsToken = this._cookieService.get('5barsToken');
    this.hostValue = this.dataservice.getHostName();
    this.baseImgUrl = environment.baseImgUrl;
  }
  
  onScrollDown () {
    if(this.orderData.length > this.infiniteScrollCount) {
      console.log('scrolled!!');
      this.infiniteScrollCount += 10;
    }
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  // gets called when user details api is done in header component
  receivedUserData($event) {
    let url = "customorder/orders?searchCriteria[filter_groups][0][filters][0][field]=customer_id&searchCriteria[filter_groups][0][filters][0][value]="+ $event.id +"&searchCriteria[filter_groups][0][filters][0][condition_type]=eq";
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data: any)=>{
      this.showLoader = false;
      let tempData = data.items.reverse();
      for (var i in tempData) {
        let temItems = tempData[i].extension_attributes.shipping_assignments[0].items;
        //temporary fix at FE to convert lat/lng to Number. This should come as no. directly from BE.
        for (var j in temItems) {
          temItems[j].extension_attributes.latitude = Number(temItems[j].extension_attributes.latitude);
          temItems[j].extension_attributes.longitude = Number(temItems[j].extension_attributes.longitude);
        }
      }
      this.orderData = tempData;
      this.ordersFetched = true;
    }, (err)=> {
      this.ordersFetched = true;
      this.showLoader = false;      
      this.showPopUpMessage = "Something went wrong while loading order history. Please try again later.";
      this.showPopUpTrue = true;
    })
  }

  calucalteTotalMonthly(items) {
    let total:number = 0;
    if(items[0].extension_attributes) {
      for(let i=0; i<items.length;i++) {
        total += Number(items[i].extension_attributes.rent_lease_fee);
      }
    }
    return total;
  }

  // order list filter fxn
  updateFilterValue() {
    if(this.searchValue != "") {
      let type = this.searchByType;
      this.filterValue = this.searchValue;
      let found = false;
      this.pageNumber = 1;
      this.filterValueObj.forEach(element => {
        if(element.name == type) {
          found = true;
          element.value = this.filterValue;
          return;
        }
      });
      if(found == false) {
        this.filterValueObj.push({
          "name" : type,
          "value" : this.searchValue
        })
      }
    }    
  }

  // removes a particular filter 
  removeFilter(filt) {
    let ind;
    this.filterValueObj.forEach((element,index) => {
      if(element.name == filt) {
        ind = index;
      }
    });
    this.filterValueObj.splice(ind,1);
    if(this.filterValueObj.length==0) {
      this.filterValueObj = [];
    }
  }

  searchValuKeyUp() {
    setTimeout(() => {
      if(this.searchValue.length==0) {
        this.filterValue = this.searchValue;
      }
    }, 200);
  }

  // to upload a new drawing
  drawingUpload(event, site, drawings) {
    let self = this;
    let target = event.target || event.srcElement;
    if (target.files.length != 0) {
      this.selectedDrawing = Array.from(target.files);
      for(let i=0; i<this.selectedDrawing.length;i++) {
        let type = this.selectedDrawing[i].type;
        let name = this.selectedDrawing[i].name;
        let reader = new FileReader();
        this.imageEntries = [];
        reader.onload = this._handleReaderLoaded.bind(this, type, name, site, drawings);
        reader.readAsBinaryString(this.selectedDrawing[i]);
      }
    }
  }

  // follow up fxn to upload a new drawing
  _handleReaderLoaded(type, name, site, drawings, readerEvt) {
    var binaryString = readerEvt.target.result;
    // let base64textString= btoa(binaryString);
    this.imageEntries.push({
      "media_type": "image",
      "label": name,
      "position": 0,
      "disabled": false,
      "types": [
        "image"
      ],
      "content": {
        "base64_encoded_data": btoa(binaryString),
        "type": type,
        "name": name
      }
    });
    if(this.imageEntries.length === this.selectedDrawing.length) {
      console.log(this.imageEntries);
      this.uploadDrawingApi(site, drawings);
    }
  }

  // follow up fxn to upload a new drawing
  uploadDrawingApi(site, drawings) {
    let url = "sitedrawings/"+site.name;
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    let data = {
      "entrys": this.imageEntries
    }
    this.showLoader = true;
    this.dataservice.fetchPostData(url, data, headerDect).subscribe((data)=>{
      this.showLoader = false;
      this.showPopUpMessage = "Image(s) uploaded successfully";
      this.showPopUpTrue = true;
      drawings.site_drawings = data;
      this.selectedDrawing = [];
    }, (err)=> {
      this.showLoader = false;      
      this.showPopUpMessage = "Something went wrong. Please try again later.";
      this.showPopUpTrue = true;
    })
  }

  // shows the model on click of view drawing
  createDrawingImgSrc(img) {
    this.drawingImgSrc = this.hostValue + this.baseImgUrl + 'pub/media/catalog/product'+img;
  }

  // to removes the existing drawing
  removeDrawing(drawing, sku, index, drawingObj) {
    console.log(sku);
    let url = "remove/sitedrawings/"+sku+"/"+drawing.id;
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.showLoader = true;
    this.dataservice.fetchDeleteData(url, headerDect).subscribe((data)=>{
      this.showLoader = false;
      if(data == false) {
        this.showPopUpMessage = "Something went wrong. Please try again later.";
        this.showPopUpTrue = true;
      } else {
        drawingObj.site_drawings.splice(index, 1);
      }
      console.log(data);
    }, err => {
      this.showLoader = false;
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "Something went wrong. Please try again later.";
      }
      this.showPopUpTrue = true;
      console.log(err);
    });
  }

  // swap site functionality takes to swap site page
  swapSite(site) {
    let swapSiteData = {
      name: site.name,
      state: site.extension_attributes.state,
      city: site.extension_attributes.city,
      order_id: site.order_id
    }
    localStorage.setItem('swapSiteData',JSON.stringify(swapSiteData));
    this.router.navigate(["orderhistory/swap-site"]);
  }
}
