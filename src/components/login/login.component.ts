import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie';
import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'site-selex-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  eyeOpen:boolean = false;
  frgtPswrdSuccessVisible:boolean=false;
  isLoginVisible: boolean = true;
  isForgotPasswordVisible: boolean = false;
  isNewUserVisible: boolean = false;
  isNewUserFormVisible: boolean = false;
  isNewUserSuccessVisible: boolean = false;
  isNewUserErrorVisible: boolean = false;
  loginErrorTrue:boolean = false;
  loginErrorText:string = '';
  resetErrorTrue:boolean = false;
  showPopUpMessage:string;  
  showPopUpTrue:boolean = false;
  showLoader:boolean = false;
  invalidUserCount:number = 0;
  captchaVal;
  
  @ViewChild('loginPassword') loginPassword;

  constructor(private router:Router, private dataservice:DataService, private _cookieService:CookieService) { }
  
  ngOnInit() {
    localStorage.clear();
    this._cookieService.removeAll();
    // clears all local storage and cookie values on initial app load
  }

  // to close the custom alert popup
  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  //toggle password's character visibility
  showHidePassword() {
    this.eyeOpen = !this.eyeOpen;
    if(this.eyeOpen) {
      this.loginPassword.nativeElement.type = "text";
    } else {
      this.loginPassword.nativeElement.type = "password";
    }
  }

  // callback function for recaptcha resolve
  resolved(event) {
    this.captchaVal = event;
    this.loginErrorTrue = false;
  }

  //gets called on sign in
  onLoginSubmit(loginForm){
    this.loginErrorTrue = false;
    if(this.invalidUserCount<2 || this.captchaVal) {  // check if user has solved the captch
      let headerDict = {
        'Content-Type': 'application/json'
      }
      let data = {
        "username": loginForm.value.email,
        "password": loginForm.value.password
      }
      this.showLoader = true;
      let url = 'integration/customer/login';
      this.dataservice.fetchPostData(url, data, headerDict).subscribe((data: any)=>{
        this.showLoader = false;
        this._cookieService.put('5barsToken', data);
        this.router.navigate(['site/home']);
      }, (err)=>{
        this.invalidUserCount++;
        if(err.target && err.target.status === 0) {
          this.showPopUpMessage = "Server connection refused. Please try after some time.";
          this.showPopUpTrue = true;
        } else if(err.target && err.target.status === 401) {
          this.showPopUpMessage = "You are not authorized to access this.";
          this.showPopUpTrue = true;
        } else {
          this.showLoader = false;
          this.loginErrorText = 'You did not sign in correctly or your account is temporarily disabled.';
          this.loginErrorTrue = true;
        }
      })
    } else {
      this.loginErrorText = 'Please solve the captcha to continue.';
      this.loginErrorTrue = true;
    }
  }
}