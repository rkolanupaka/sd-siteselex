import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { CookieService } from "ngx-cookie";
import { DataService } from "../../common/services/data.service";
import { FormatSitesResponse } from "../../common/services/format-sites-response.service";
import {FormControl, Validators} from '@angular/forms';
import { environment } from '../../environments/environment';
import { callLifecycleHooksChildrenFirst } from "@angular/core/src/view/provider";

@Component({
  selector: "app-site-search",
  templateUrl: "./site-search.component.html",
  styleUrls: ["./site-search.component.scss"]
})
export class SiteSearchComponent implements OnInit {

  selectedSiteId = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z0-9]{9}(,[a-zA-Z0-9]{9})*'),
    Validators.maxLength(99)
  ]);

  allSitesOptionalAdd: boolean = false;
  citySelected = "";
  citylist = [];
  citywdoutinvtrylist = [];
  chosenSites = [];
  chosenSitesTemp = [];
  disableFileUpload: boolean = true;
  fiveBarsToken: string;
  fiveBarsCartId: string;
  hostValue: string;
  baseUrl: string;
  parseInt = parseInt;
  sitesZeroCartCount: number = 0;
  stateSelected = "";
  siteIdError: boolean = false;
  siteIdErrorText: boolean = false;
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  selectedStateText: string;
  selectedCityText: string;
  statecitylist = [];
  siteIdData = [];
  showLoader: boolean = false;
  selectedStateValue;
  showUploadedAvailableSiteDet: boolean = false;
  showUploadedNonAvailableSiteDet: boolean = false;
  showUploadedWaitlistSiteDet: boolean = false;
  searchValue: string = "";
  searchValueFilter: string = "";
  searchValueCounter: number = 0;
  searchByType = "Site ID";
  uploadedSiteIds: string = "";
  uploadedFileName: string = "";
  uploadedFileFlag: boolean = false;
  uploadSiteIdError: boolean = false;
  uploadSiteIdErrorText: boolean = false;
  uploadedSitesInvalid = [];
  validSiteError: boolean = true;
  chosenSitesAvailableFiltered: any = [];
  chosenSitesWaitFiltered: any = [];
  mySiteMap:any;

  @ViewChild("fileUploadInput") fileUploadInput;

  @ViewChild("agmMapNested") myMap;

  // was facing issue with google map to set center and found the below solution. Might remove in future
  @HostListener("window:resize")
  onWindowResize() {
    if (this.myMap) {
      this.myMap.triggerResize().then(() => {
        console.log("resized");
        (this.myMap as any)._mapsWrapper.setCenter({
          lat: this.markerInfoWindowObj.markerInfoWindowLat,
          lng: this.markerInfoWindowObj.markerInfoWindowLng
        });
      });
    }
  }

  activeScreen: string = "findSiteByInput";

  openNav: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private dataservice: DataService,
    private _cookieService: CookieService,
    private router: Router,
    private formatSitesResponse: FormatSitesResponse
  ) {
    this.baseUrl = environment.baseUrl;
    this.route.queryParams.subscribe(params => {   // set active screen to uploadsites or findSiteByInput and show sections based on that
      this.activeScreen = params.type;
    });
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.hostValue = this.dataservice.getHostName();
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    this.loadStateAndCities();    // loads states and cities on page load
    this.loadSitesFromLocalWdoutCart();   // loads sites from localstorage on page load to show in below section
  }

  // loads states and cities on page load
  loadStateAndCities() {
    let url = "state/cities";
    let headerDect = {
      "Content-Type": "application/json"
    };
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe(
      (data:any) => {
        this.showLoader = false;
        this.statecitylist = data.items.sort(function(a, b) {
          return a.state.localeCompare(b.state);
        });
        if (localStorage.getItem("preStateCity") != undefined) { //set state/city in local storage to preselect it on page load
          this.preselectStateCity();
        }
        if(!this._cookieService.get('5barsCartId')) {
          this.createCartId();
        }
      },
      err => {
        this.showLoader = false;
        this.showPopUpMessage = "Something went wrong while loading states. Please try again later.";
        this.showPopUpTrue = true;
      }
    );
  }

  loadSitesFromLocalWdoutCart() {
    if (localStorage.getItem("selectedSiteDataWdotCart")) {
      this.chosenSites = JSON.parse(localStorage.getItem("selectedSiteDataWdotCart"));
    }
  }

  preselectStateCity() {
    let stateCityPreData = localStorage.getItem("preStateCity").split("***");
    this.stateSelected = stateCityPreData[0];
    this.onStateChange(stateCityPreData[0]);
    this.citySelected = stateCityPreData[1];
    this.selectedCityText = stateCityPreData[1];
    if (this.activeScreen == "findSiteByInput" && this.selectedCityText != "Select City") {
      this.loadGoogleMaps();
    }
  }

  // to place and order need to create cart ID. So this is called if cart id not already present
  createCartId() {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    let data = {};
    let url = "carts/mine";
    this.dataservice.fetchPostData(url, data, headerDict).subscribe(
      (data:any) => {
        this._cookieService.put("5barsCartId", data);
        this.fiveBarsCartId = data;
        if(localStorage.getItem('duplicateSku')) {
          this.selectedSiteId.setValue(localStorage.getItem('duplicateSku'));
          localStorage.removeItem('duplicateSku');
          this.addChosenSite();
        }
      },
      err => {
        this.showPopUpMessage = "There was some issue creating your Cart ID. Request you to login again.";
        this.showPopUpTrue = true;
        setTimeout(() => {
          this.router.navigate([""]);
        }, 4000);
      }
    );
  }

  // on change of state update the city list
  onStateChange(state) {
    if (state.value != undefined) {
      this.selectedStateText = state.value;
    } else {
      this.selectedStateText = state;
    }
    var stateCode = this.stateSelected;
    if (stateCode == "") {
      this.citylist = [];
      this.citywdoutinvtrylist = [];
      this.citySelected = "";
      this.selectedCityText = "Select City";
    } else {
      for (let a of this.statecitylist) {
        if (a.state == stateCode) {
          this.citylist = a.cities.sort(function(a, b) {
            return a.localeCompare(b);
          });
          this.citywdoutinvtrylist = a.cities_without_inventory.sort(function(a, b) {
            return a.localeCompare(b);
          });
        }
      }
      this.citySelected = "";
    }
  }

  // on city change update map sites
  onCityChange(city) {
    this.resetAllErrors();
    this.selectedCityText = city.value;
    this.selectedSiteId.setValue("");
    localStorage.setItem(
      "preStateCity",
      this.stateSelected + "***" + this.citySelected
    );
    if (this.activeScreen == "findSiteByInput" && this.selectedCityText != "Select City") {
      this.loadGoogleMaps();
    }
  }

  resetAllErrors() {
    this.siteIdError = false;
    this.uploadSiteIdError = false;
    this.uploadSiteIdError = false;
  }

  // bulk file upload
  fileUpload(event) {
    let self = this;
    let target = event.target || event.srcElement;
    if (target.files.length != 0) {
      this.commonFileUpload(target.files[0]);
    }
  }

  // reseting values on removing file upload
  removeUploadedSitesFile() {
    this.allSitesOptionalAdd = false;
    this.uploadedFileName = "";
    this.uploadedSiteIds = "";
    this.fileUploadInput.nativeElement.value = "";
    this.uploadedFileFlag = false;
    this.chosenSitesTemp = [];
  }

  // if user has drag n dropped the file this event is fired from the directive
  onFileChangeInDirective(evt) {
    this.commonFileUpload(evt[0]);
  }

  // api call to search all the sites uploaded
  commonFileUpload(fileDetail) {
    this.resetAllErrors();
    if (this.citySelected == "" || this.stateSelected == "") {
      this.fileUploadInput.nativeElement.value = "";
      this.uploadedSiteIds = "";
      this.uploadSiteIdError = true;
      this.uploadSiteIdErrorText = false;
      return;
    }
    this.siteIdError = false;
    let reader = new FileReader();
    let self = this;
    self.selectedSiteId.setValue("");
    reader.readAsText(fileDetail);
    reader.onload = function() {
      self.uploadedFileName = fileDetail.name;
      self.uploadedSiteIds = reader.result
        .trim()
        .replace(/ /g, "")
        .replace(/\r?\n|\r/g, "")
        .replace(/,/g, ", ")
        .toUpperCase();
      self.addChosenSite();
    };
  }

  // download kmz method starts
  downloadKmz() {
    if (
      this.selectedCityText == undefined ||
      this.selectedStateText == undefined ||
      this.selectedStateText == "Select State" ||
      this.selectedCityText == "Select City"
    ) {
      this.showPopUpMessage = "Please select state and city";
      this.showPopUpTrue = true;
    } else {
      this.showLoader = true;
      let headerDect = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      let url = "report/site/location/" + this.selectedStateText +
        "/" + this.selectedCityText;
      this.dataservice.fetchPlainData(url, headerDect).subscribe(
        (data:any) => {
          this.showLoader = false;
          if (data == '"false"') {
            this.showPopUpMessage = "No data available for this selection.";
            this.showPopUpTrue = true;
          } else {
            window.open(
              this.hostValue + this.baseUrl +
                "report/site/location/" +
                this.selectedStateText +
                "/" +
                this.selectedCityText,
              "_self"
            );
          }
        },
        err => {
          this.showLoader = false;
          if (err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please try again";
          }
          this.showPopUpTrue = true;
        }
      );
    }
  }
  // download kmz method ends

  // add entered sites method starts
  addChosenSite() {
    // if user is adding sites using bulk upload we don't need validations etc we so small code and return
    if (this.uploadedFileFlag) {
      this.chosenSites = this.chosenSites.concat(this.chosenSitesTemp);
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      this.removeUploadedSitesFile();
      return;
    }

    // if user is not using bulk upload we validate the site
    this.uploadedSitesInvalid = [];
    let enteredSitesString =
      this.uploadedSiteIds != "" ? this.uploadedSiteIds.replace(/ /g, "") : this.selectedSiteId.value.replace(/ /g, "");
    let enteredSitesArray =
      this.uploadedSiteIds != ""
        ? this.uploadedSiteIds.replace(/ /g, "").split(",")
        : this.selectedSiteId.value.replace(/ /g, "").split(",");
    let enteredSitesAlreadyArray = [];
    let allSitesValid = true;
    let enteredSitesArrayTemp = enteredSitesArray.map(x => x);
    let alreadyAddedCounter = 0;
    let inValidorUnavailableSites = [];
    enteredSitesArray.forEach((site, index) => {
      if (site.trim().length != 9) {
        allSitesValid = false;
      }
      let isAlreadyAdded = this.checkIfAlreadyAdded(site.trim());
      if (isAlreadyAdded) {
        enteredSitesArrayTemp.splice(index - alreadyAddedCounter, 1);
        enteredSitesAlreadyArray.push(site.trim());
        enteredSitesString = enteredSitesString.replace(site.trim(), "");
        alreadyAddedCounter++;
      }
    });

    if (enteredSitesString.replace(/[,]/, "") == "") {
      if (enteredSitesArray.length > 1) {
        this.showPopUpMessage = "Sites you are trying to add are already added and cannot be added again.";
      } else {
        this.showPopUpMessage = "Site you are trying to add is already added and cannot be added again.";
      }
      this.showPopUpTrue = true;
      return false;
    }

    // if all sites are valid we make an api call to get more details about those sites and add it to bottom section
    if (allSitesValid) {
      this.selectedSiteId.setValue("");
      this.showLoader = true;
      var url =
        "products?" +
        "searchCriteria[filter_groups][0][filters][0][field]=sku&" +
        "searchCriteria[filter_groups][0][filters][0][value]=" +
        enteredSitesString +
        "&" +
        "searchCriteria[filter_groups][0][filters][0][condition_type]=in&" +
        "searchCriteria[filter_groups][1][filters][0][field]=state&" +
        "searchCriteria[filter_groups][1][filters][0][value]=" +
        this.selectedStateText +
        "&" +
        "searchCriteria[filter_groups][1][filters][0][condition_type]=like&" +
        "searchCriteria[filter_groups][2][filters][0][field]=city&" +
        "searchCriteria[filter_groups][2][filters][0][value]=" +
        this.selectedCityText +
        "&" +
        "searchCriteria[filter_groups][2][filters][0][condition_type]=like&fields=items[sku,name,status,custom_attributes,price],total_count";

      const headerDict = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.fiveBarsToken
      };
      this.dataservice.fetchData(url, headerDict).subscribe(
        (data:any) => {
          let tempResponseSites = data.items;
          if (tempResponseSites != null && tempResponseSites.length > 0) {
            //we get lot of extra info and onordered format. So we make a call to format function in common/format-sites-response.service folder to format it as per needed in front end
            let formattedSites = this.formatSitesResponse.getSitesFormatted(tempResponseSites, "all");
            if (this.uploadedSiteIds == "") {
              this.chosenSites = this.chosenSites.concat(formattedSites);
            } else {
              this.chosenSitesTemp = this.chosenSitesTemp.concat(formattedSites);
            }

            if (this.uploadedSiteIds == "") {
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
            } else {
              if (this.chosenSitesTemp.length > 0) {
                this.chosenSitesAvailableFiltered = this.chosenSitesTemp.filter(item => item.reserved == false);
                this.chosenSitesWaitFiltered = this.chosenSitesTemp.filter(item => item.reserved == true);
                this.showUploadedAvailableSiteDet = false;
                this.showUploadedNonAvailableSiteDet = false;
                this.showUploadedWaitlistSiteDet = false;
                this.uploadedFileFlag = true;
              }
            }

            console.log(this.chosenSitesTemp);
          }
          this.showLoader = false;

          //error handling for site search starts
          let errorChosenSitesTemp = [];
          if (this.uploadedSiteIds == "") {
            errorChosenSitesTemp = this.chosenSites;
          } else {
            errorChosenSitesTemp = this.chosenSitesTemp;
          }
          enteredSitesArrayTemp.forEach((site, index) => {
            let added = false;
            errorChosenSitesTemp.forEach(element => {
              if (element.uniqueId.toLowerCase() == site.toLowerCase()) {
                added = true;
              }
            });
            if (added == false) {
              inValidorUnavailableSites.push(site);
            }
          });

          let fullAlreadyAddedSiteString: string;
          if (enteredSitesAlreadyArray.length > 0) {
            fullAlreadyAddedSiteString = enteredSitesAlreadyArray[0];
            if (enteredSitesAlreadyArray.length > 1) {
              enteredSitesAlreadyArray.forEach((site, index) => {
                if (index != 0) {
                  fullAlreadyAddedSiteString += ", " + site;
                }
              });
              fullAlreadyAddedSiteString =
                "Sites " + fullAlreadyAddedSiteString + " are already added and cannot be added again.";
            } else {
              fullAlreadyAddedSiteString =
                "Site " + fullAlreadyAddedSiteString + " is already added and cannot be added again.";
            }
          }

          let fullInvalidSIteString: string;
          if (inValidorUnavailableSites.length > 0) {
            fullInvalidSIteString = inValidorUnavailableSites[0];
            if (inValidorUnavailableSites.length > 1) {
              inValidorUnavailableSites.forEach((site, index) => {
                if (index != 0) {
                  fullInvalidSIteString += ", " + site;
                }
              });
              fullInvalidSIteString = "Sites " + fullInvalidSIteString + " are either invalid or not available.";
            } else {
              fullInvalidSIteString = "Site " + fullInvalidSIteString + " is either invalid or not available.";
            }
          }

          if (this.uploadedSiteIds != "") {
            this.uploadedSitesInvalid = inValidorUnavailableSites.concat(enteredSitesAlreadyArray);
            if (this.uploadedSitesInvalid.length > 0) {
              this.showUploadedAvailableSiteDet = false;
              this.showUploadedNonAvailableSiteDet = false;
              this.showUploadedWaitlistSiteDet = false;
              this.uploadedFileFlag = true;
            }
          } else {
            if (fullInvalidSIteString != undefined && fullAlreadyAddedSiteString != undefined) {
              this.showPopUpMessage = fullInvalidSIteString + "<br>" + fullAlreadyAddedSiteString;
              this.showPopUpTrue = true;
            } else if (fullInvalidSIteString != undefined) {
              this.showPopUpMessage = fullInvalidSIteString;
              this.showPopUpTrue = true;
            } else if (fullAlreadyAddedSiteString != undefined) {
              this.showPopUpMessage = fullAlreadyAddedSiteString;
              this.showPopUpTrue = true;
            }
          }
          //error handling for site search ends

          window.scrollTo(0, document.body.scrollHeight);
        },
        err => {
          this.showPopUpMessage = "something went wrong while fetching the Sites. Please try again.";
          this.showPopUpTrue = true;
          this.selectedSiteId.setValue("");
          this.showLoader = false;
        }
      );
    } else {
      if (this.uploadedSiteIds != "") {
        this.uploadSiteIdError = true;
        this.uploadSiteIdErrorText = true;
        this.uploadedSiteIds = "";
        this.fileUploadInput.nativeElement.value = "";
      } else {
        this.siteIdError = true;
        this.siteIdErrorText = true;
      }
    }
  }
  // add enetered sites method ends

  // function to check if site is already added
  checkIfAlreadyAdded(siteId) {
    let added = false;
    this.chosenSites.forEach(element => {
      if (element.uniqueId.toLowerCase() == siteId.toLowerCase()) {
        added = true;
      }
    });
    return added;
  }

  // remove site method starts
  removeSite(site) {
    if (site.item_id) {
      let headerDict = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      this.showLoader = true;
      let url = "";
      if (site.reserved) {
        url = "waitlist/delete/" + site.uniqueId;
      } else {
        url = "carts/mine/items/" + site.item_id;
      }
      this.dataservice.fetchDeleteData(url, headerDict).subscribe(
        data => {
          this.showLoader = false;
          this.chosenSites.splice(this.chosenSites.indexOf(site), 1);
          localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
          if (localStorage.getItem("selectedSiteDataWidCart")) {
            let withCartSitesTemp = JSON.parse(localStorage.getItem("selectedSiteDataWidCart"));
            let spliceIdex: number;
            withCartSitesTemp.forEach((element, index) => {
              if (element.uniqueId == site.uniqueId) {
                spliceIdex = index;
              }
            });
            if (spliceIdex != undefined) {
              withCartSitesTemp.splice(spliceIdex, 1);
              localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(withCartSitesTemp));
            }
          }
          if (this.chosenSites.length == 0) {
            this.sitesZeroCartCount++;
          }
          this.searchValueCounter++;
        },
        err => {
          this.showLoader = false;
          if (err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong..";
          }
          this.showPopUpTrue = true;
        }
      );
    } else {
      this.chosenSites.splice(this.chosenSites.indexOf(site), 1);
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      if (this.chosenSites.length == 0) {
        this.sitesZeroCartCount++;
      }
      this.searchValueCounter++;
    }
  }
  // remove site method ends

  // if optional fees option changes only at search page level then no need for update API callLifecycleHooksChildrenFirst. However if we come back from payemnt page and update anything in site we make api call to update it at cart level
  changeOptionalFeeStatus(index, event) {
    let site = this.chosenSites[index];
    let tempEvent = event;
    if (site.item_id) {
      let surveyFlag = 0;
      let conduitFlag = 0;

      if(event.name.indexOf('optionalFeeStatus') > -1) {
        if (site.optionalFeeStatus) {
          surveyFlag = 0;
        } else {
          surveyFlag = 1;
        }
        conduitFlag = site.optionalConduitStatus?1:0;
      } else {
        if(site.optionalConduitStatus) {
          conduitFlag = 0;
        } else {
          conduitFlag = 1;
        }
        surveyFlag = site.optionalFeeStatus?1:0;
      }
      
      let headerDict = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      let url = "custom/carts/mine/items";
      let siteData = {
        surveyLeaseFlag: surveyFlag,
        hasConduitRequested: conduitFlag,
        cart_item: {
          item_id: site.item_id,
          quote_id: this._cookieService.get('5barsCartId'),
          sku: site.uniqueId,
          qty: 1
        }
      };
      this.showLoader = true;
      this.dataservice.fetchPutData(url, siteData, headerDict).subscribe(
        data => {
          let withCartSitesTemp = JSON.parse(localStorage.getItem("selectedSiteDataWidCart"));
          withCartSitesTemp.forEach((element, index) => {
            if (element.uniqueId == site.uniqueId) {
              if(event.name.indexOf('optionalFeeStatus') > -1) {
                element.optionalFeeStatus = !element.optionalFeeStatus;
              } else {
                element.optionalConduitStatus = !element.optionalConduitStatus;
              }
            }
            localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(withCartSitesTemp));
          });
          this.changeOptionalFeeStatusCommon(index, event);
          this.showLoader = false;
        },
        error => {
          this.showLoader = false;
          this.showPopUpMessage = "something went wrong. Please try again";
          this.showPopUpTrue = true;
          tempEvent.checked = !tempEvent.checked;
        }
      );
    } else {
      this.changeOptionalFeeStatusCommon(index, event);
    }
  }

  changeOptionalFeeStatusCommon(index, event) {
    let site = this.chosenSites[index];
    if(event.name.indexOf('optionalFeeStatus') > -1) {
      site.optionalFeeStatus = !site.optionalFeeStatus;
      setTimeout(() => {
        if (event.checked) {
          site.one_time_fee_price = Number(site.site_selection_fee) + Number(site.site_survey_lease_fee);
        } else {
          site.one_time_fee_price = Number(site.site_selection_fee);
        }
        localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      }, 100);
    } else {
      site.optionalConduitStatus = !site.optionalConduitStatus; 
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
    } 
  }

  changeAllOptionalFeeStatus(event) {
    if (event.checked) {
      this.chosenSitesTemp.forEach(element => {
        element.optionalFeeStatus = true;
        element.one_time_fee_price = Number(element.site_selection_fee) + Number(element.site_survey_lease_fee);
      });
    } else {
      this.chosenSitesTemp.forEach(element => {
        element.optionalFeeStatus = false;
        element.one_time_fee_price = Number(element.site_selection_fee);
      });
    }
  }

  // for bulk upload sites only to preselect flag
  checkAllSitesReservedFlag() {
    let temp = this.chosenSites.filter(item => item.reserved == false);
    if (temp.length > 0) {
      return true;
    }
    return false;
  }

  // different api call if user has added only wait list sites i.e. sites which are in reserved status
  allSitesAddedToWaitListFlag = false;
  addAllSitesToWaitList() {
    let counter = 0;
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    this.chosenSites.forEach(element => {
      if (element.item_id == undefined) {
        let url = "waitlist/add/" + element.uniqueId;
        this.showLoader = true;
        this.dataservice.fetchPostData(url, {}, headerDict).subscribe(
          data => {
            this.showLoader = false;
            this.chosenSites.forEach(element2 => {
              if (element2.uniqueId == element.uniqueId) {
                element2.item_id = counter + 1;
              }
            });
            counter++;
            if (counter == this.chosenSites.length) {
              this.showLoader = false;
              this.chosenSites = [];
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
              localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
              this.allSitesAddedToWaitListFlag = true;
            }
          },
          error => {
            this.showPopUpMessage = "something went wrong...";
            this.showPopUpTrue = true;
            this.showLoader = false;
          }
        );
      } else {
        counter++;
      }
    });
    if (counter == this.chosenSites.length) {
      this.showLoader = false;
      this.chosenSites = [];
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
      this.allSitesAddedToWaitListFlag = true;
    }
  }

  // when click on proceed to checkout we make api call individually for every site based on the status
  reserveSites(e) {
    let counter = 0;  // counter to track if all the sites has been successfully added(included increment for previously added sites)
    let errorOccured: boolean = false;
    this.chosenSites.forEach(element => {
      if (element.item_id == undefined) { // if site was already added to cart i.e case when user has come back from payment page, we don't make api call as that site is already added to cart
        let headerDict = {
          "Content-Type": "application/json",
          authorization: "Bearer " + this.fiveBarsToken
        };
        let data = {};
        if (element.reserved) {  // api call for wait list site
          let url = "waitlist/add/" + element.uniqueId;
          this.showLoader = true;
          this.dataservice.fetchPostData(url, data, headerDict).subscribe(
            data => {
              this.chosenSites.forEach(element2 => {
                if (element2.uniqueId == element.uniqueId) {
                  element2.item_id = counter + 1;
                }
              });
              counter++;
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
              if (counter == this.chosenSites.length) {
                this.showLoader = false;
                localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
                this.router.navigate(["site/payment"]);
              }
              if (errorOccured) {
                this.showLoader = false;
              }
            },
            error => {
              errorOccured = true;
              if (error.message != undefined) {
                if (error.message.match(/"([^"]+)"/) != undefined) {
                  this.showPopUpMessage =
                    "Sorry " +
                    JSON.parse(error.responseText).message.match(/"([^"]+)"/)[1] +
                    " was reserved by someone just now and not available for reservation";
                } else {
                  this.showPopUpMessage = error.message;
                }
              } else {
                this.showPopUpMessage = "something went wrong...";
              }
              this.showPopUpTrue = true;
              this.showLoader = false;
              return false;
            }
          );
        } else {  // api call for available site
          let url = "carts/mine/items";
          let tempStatus = element.optionalFeeStatus + "";
          let tempConduitStatus = element.optionalConduitStatus + "";
          let siteData = {
            survey_lease_selected: tempStatus,
            has_conduit_requested: tempConduitStatus,
            cart_item: {
              quote_id: this._cookieService.get('5barsCartId'),
              sku: element.uniqueId,
              qty: 1
            }
          };
          this.showLoader = true;
          this.dataservice.fetchPostData(url, siteData, headerDict).subscribe(
            (data:any) => {
              this.chosenSites.forEach(element => {
                if (element.uniqueId == data.sku) {
                  element.item_id = data.item_id;
                }
              });
              counter++;
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
              if (counter == this.chosenSites.length) {
                this.showLoader = false;
                localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
                this.router.navigate(["site/payment"]);
              }
              if (errorOccured) {
                this.showLoader = false;
              }
            },
            error => {
              errorOccured = true;
              if (error.message != undefined) {
                if (error.message.match(/"([^"]+)"/) != undefined) {
                  this.showPopUpMessage =
                    "Sorry " +
                    JSON.parse(error.responseText).message.match(/"([^"]+)"/)[1] +
                    " was reserved by someone just now and not available for reservation";
                } else {
                  this.showPopUpMessage = error.message;
                }
              } else {
                this.showPopUpMessage = "something went wrong...";
              }
              this.showPopUpTrue = true;
              this.showLoader = false;
              return false;
            }
          );
        }
      } else {
        counter++;
      }
    });
    if (counter == this.chosenSites.length) {
      this.showLoader = false;
      localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
      this.router.navigate(["site/payment"]);
    }
  }

  // below code is for agm map integration
  markers: any = [];
  // google maps zoom level
  zoom: number = 15;

  // initial center position for the map
  lat: number = 38.79930767201779;
  lng: number = -77.12911152370515;
  infoWindowIsOpen: boolean = false;
  infoWindowZoom: number = 15;
  mapLoading: boolean = false;
  markerInfoWindowObj: any = {
    markerInfoWindowLat: 0,
    markerInfoWindowLng: 0,
    markerInfoWindowSku: "",
    markerInfoWindowPoleType: "",
    markerInfoWindowLocationType: "",
    markerInfoWindowElectical: "",
    markerInfoWindowStatus: ""
  };

  loadGoogleMaps() {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    
    let url =  "../assets/search-data/" 
    + this.selectedStateText + "_" + this.selectedCityText+".txt";
    this.mapLoading = true;
    this.dataservice.fetchLocalData(url).subscribe(
      data => {
        this.zoom = 15;
        this.mapLoading = false;
        this.markers = data;
        this.lat = this.markers[0].lat;
        this.lng = this.markers[0].long;
        console.log("success");
      },
      err => {
        console.log(err);
        this.mapLoading = false;
      }
    );
  }

  boundsChange(event) {
    console.log(event);
  }

  centerChange(event) {
    console.log(event);
  }

  resizeCounter = 0;
  markerInfoWindowSkuIsOpen = false;
  markerClicked(siteData) {
    console.log(siteData);
    this.infoWindowZoom = 15;
    this.markerInfoWindowObj.markerInfoWindowSku = siteData.sku;
    this.markerInfoWindowObj.markerInfoWindowLng = siteData.long;
    this.markerInfoWindowObj.markerInfoWindowLat = siteData.lat;
    this.markerInfoWindowObj.markerInfoWindowStatus = siteData.sChk;
    this.markerInfoWindowObj.markerInfoWindowPoleType = siteData.pCls;
    this.markerInfoWindowObj.markerInfoWindowLocationType = siteData.cArea;
    this.infoWindowIsOpen = true;
    let resizeTimeOut;
    if (this.resizeCounter == 0) {
      resizeTimeOut = setTimeout(() => {
        let event = document.createEvent("Event");
        if (typeof Event === "function") {
          event = new Event("resize");
        } else {
          event = document.createEvent("Event");
          event.initEvent("resize", true, true);
        }
        window.dispatchEvent(event);
        this.markerInfoWindowSkuIsOpen = true;
      }, 100);
    } else if (this.resizeCounter == 1) {
      clearTimeout(resizeTimeOut);
    }
    this.resizeCounter++;
  }

  infoWindowClose(event) {
    this.infoWindowIsOpen = false;
  }

  mapReady(map) {
    this.mySiteMap = map;
  }

  mapReset() {
    this.mySiteMap.setCenter({ lat: this.lat, lng: this.lng });
    this.mySiteMap.setZoom(this.zoom);
  }
  // maps css ends

  addSIteFromMap() {
    this.selectedSiteId.setValue(this.markerInfoWindowObj.markerInfoWindowSku);
    this.addChosenSite();
    this.infoWindowIsOpen = false;
  }
}
