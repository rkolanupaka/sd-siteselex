import { Component, OnInit } from '@angular/core';
import { DataService } from '../../common/services/data.service';
import { CookieService } from 'ngx-cookie';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-site-requests',
  templateUrl: './site-requests.component.html',
  styleUrls: ['./site-requests.component.scss']
})
export class SiteRequestsComponent implements OnInit {
  siteFetched = false;
  siteRequests = [];
  siteApproved = [];
  siteRejected = [];
  fiveBarsToken:string;
  showAddSiteForm = false;

  showLoader:boolean = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  rejectSiteTrue = false;
  rejectedSiteSelected:any;
  rejectReason:string = '';

  siteFormModel = { 
    latitude: '',
    longitude: '',
    locType: '',
    poleType: '',
    siteSelectionFee: '',
    rentLeaseMonthlyFee: '',
    siteSurveyLeaseFee: '',
    state: '',
    city: '',
    requestId: '',
    customerId: ''
  };

  maxDate = new Date();
  startDateModel = new FormControl(new Date(new Date().setMonth(new Date().getMonth() - 1)));  //sets initial value to 1 month before the current date

  constructor(
    private dataservice: DataService,
    private _cookieService:CookieService
  ) { }

  ngOnInit() {
    this.fiveBarsToken = this._cookieService.get('5barsToken');
    this.getSiteRequests();
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  // get the list of all sites requested since the date in the datepicker
  getSiteRequests() {
    let startDate = this.startDateModel.value.getFullYear()+"-"+(this.startDateModel.value.getMonth()+1)+"-"+this.startDateModel.value.getDate() + ' 00:00:00';
    let url = "sitemanagement/siteRequestItems/"+ startDate;
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data)=>{
      this.showLoader = false;
      let tempData:any = data;
      this.siteRequests = tempData.filter(item=> item.status === 'REQUESTED');
      this.siteApproved = tempData.filter(item=> item.status === 'APPROVED');
      this.siteRejected = tempData.filter(item=> item.status === 'REJECTED');
      this.siteFetched = true;
    }, (err)=> {
      this.siteFetched = true;
      this.showLoader = false;      
      this.showPopUpMessage = "Something went wrong while loading site requests. Please try again later.";
      this.showPopUpTrue = true;
    })
  }

  // makes an api call again on date change
  onDateChanged() {
    if(this.startDateModel.value != null && this.startDateModel.value != '') {
      setTimeout(()=> {
        this.getSiteRequests();
      },100);
    }
  }

  // open a form for admin with pre filled requested data on clicking of approve site
  createSite(site) {
    this.siteFormModel = {
      latitude: site.latitude,
      longitude: site.longitude,
      locType: site.city_area,
      poleType: site.pole_class,
      siteSelectionFee: '',
      rentLeaseMonthlyFee: '',
      siteSurveyLeaseFee: '',
      state: site.state,
      city: site.city,
      requestId: site.request_id,
      customerId: site.customer_id
    };
    this.showAddSiteForm = true;
    window.scroll({
      top: 200,
      behavior: "smooth"
    });
  }

  // makes an api call to finally approve site on submit
  createSiteSubmit() {
    const URL = "siteRequests/adminApprove";
    const DATA: any = {
      siteRequestedInfo: {
        requestId: this.siteFormModel.requestId,
        customerId: this.siteFormModel.customerId,
        product: {
          custom_attributes: [
            {
              attribute_code: "latitude",
              value: this.siteFormModel.latitude + ''
            },
            {
              attribute_code: "state",
              value: this.siteFormModel.state
            },
            {
              attribute_code: "city",
              value: this.siteFormModel.city
            },
            {
              attribute_code: "longitude",
              value: this.siteFormModel.longitude + ''
            },
            {
              attribute_code: "pole_class",
              value: this.siteFormModel.poleType
            },
            {
              attribute_code: "rent_lease_fee",
              value: this.siteFormModel.rentLeaseMonthlyFee
            },
            {
              attribute_code: "site_selection_fee",
              value: this.siteFormModel.siteSelectionFee
            },
            {
              attribute_code: "site_survey_lease_fee",
              value: this.siteFormModel.siteSurveyLeaseFee
            },
            {
              attribute_code: "city_area",
              value: this.siteFormModel.locType
            },
            {
              attribute_code: "electrical_service_field",
              value: "210v"
            },
            {
              attribute_code: "reserved",
              value: 0
            }
          ]
        }
      }
    };

    const headerDict = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.fiveBarsToken
    };

    this.showLoader = true;
    this.dataservice.fetchPostData(URL, DATA, headerDict).subscribe(
      data => {
        this.showLoader = false;
        this.showPopUpMessage = "Site added successfully";
        this.showPopUpTrue = true;
        this.getSiteRequests();  // after successful approval makes the call again to update the list on the page
      },
      err => {
        this.showLoader = false;
        if (err.message && err.message != "") {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "something went wrong. Please try again";
        }
        this.showPopUpTrue = true;
      }
    );
    this.showAddSiteForm = false;
  }

  // shows up the section to select reason for rejection on click of reject site
  RejectSite(site) {
    this.rejectReason = '';
    this.rejectedSiteSelected = site;
    this.rejectSiteTrue = true;
  }

  // makes an api call on final site reject submit and updates the list on the page
  rejectSiteSubmit() {
    console.log(this.rejectReason);
    const URL = "siteRequests/adminReject";
    const DATA: any = {
      siteRequestedInfo: {
        requestId: this.rejectedSiteSelected.request_id,
        customerId: this.rejectedSiteSelected.customer_id,
        rejectedReason: this.rejectReason,
        product: {
          custom_attributes: [
            {
              attribute_code: "latitude",
              value: this.rejectedSiteSelected.latitude + ''
            },
            {
              attribute_code: "state",
              value: this.rejectedSiteSelected.state
            },
            {
              attribute_code: "city",
              value: this.rejectedSiteSelected.city
            },
            {
              attribute_code: "longitude",
              value: this.rejectedSiteSelected.longitude + ''
            },
            {
              attribute_code: "pole_class",
              value: this.rejectedSiteSelected.pole_class
            },
            {
              attribute_code: "city_area",
              value: this.rejectedSiteSelected.city_area
            },
            {
              attribute_code: "electrical_service_field",
              value: "210v"
            }
          ]
        }
      }
    };

    const headerDict = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.fiveBarsToken
    };

    this.showLoader = true;
    this.dataservice.fetchPostData(URL, DATA, headerDict).subscribe(
      data => {
        this.showLoader = false;
        console.log(data);
        this.showPopUpMessage = "Site rejected successfully";
        this.showPopUpTrue = true;
        this.rejectSiteTrue = false;
        this.getSiteRequests();
      },
      err => {
        this.showLoader = false;
        if (err.message && err.message != "") {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "something went wrong. Please try again";
        }
        this.showPopUpTrue = true;
        this.rejectSiteTrue = false;
      }
    );
  }
}
