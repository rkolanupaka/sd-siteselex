import { Component, OnInit } from '@angular/core';

import { DataService } from '../../common/services/data.service';
import {FormControl} from '@angular/forms';
import { environment } from '../../environments/environment';

@Component({
  selector: 'site-selex-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})

export class ReportsComponent implements OnInit {

  fiveBarsToken:string;
  hostValue:string;
  baseUrl:string;
  reportType = "";
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  showLoader: boolean = false;

  date = new FormControl(new Date(new Date().setMonth(new Date().getMonth() - 1)));
  date1 = new FormControl(new Date());
  maxDate = new Date();

  constructor(private dataservice: DataService) {
    this.baseUrl = environment.baseUrl;
  }

  ngOnInit() {
    this.hostValue = this.dataservice.getHostName();
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  // submits the report form and downloads the report on valid response
  reportsFormSubmit(reportForm) {
    if(this.reportType == "" || this.date.invalid || this.date1.invalid) {
      this.showPopUpMessage = "All fields are mandatory.";
      this.showPopUpTrue = true;
    } else {
      let startDate = this.date.value.getFullYear()+"-"+(this.date.value.getMonth()+1)+"-"+this.date.value.getDate();
      let endDate = this.date1.value.getFullYear()+"-"+(this.date1.value.getMonth()+1)+"-"+this.date1.value.getDate();
      let reportType = reportForm.value.reportType;
      let url = 'report/'+reportType+'/'+startDate+' 00:00:00/'+endDate+' 23:59:59';
      this.showLoader = true;
      let headerDect = {
        "Content-Type": "application/json"
      }
      this.dataservice.fetchPlainData(url, headerDect).subscribe(
        (data:any)=>{
          this.showLoader = false;
          if(data == '"false"') {
            this.showPopUpMessage = "No data available for this selection.";
            this.showPopUpTrue = true;
          } else {
            window.open(this.hostValue + this.baseUrl + 'report/'+reportType+'/'+startDate+' 00:00:00/'+endDate+' 23:59:59', "_self");
          }
        },(err)=> {
          this.showLoader = false;
          if(err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please try again";
          }
          this.showPopUpTrue = true;
      });
    }
  }
}
