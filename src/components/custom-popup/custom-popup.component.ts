import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'site-selex-custom-popup',
  templateUrl: './custom-popup.component.html',
  styleUrls: ['./custom-popup.component.scss']
})
export class CustomPopupComponent {
  
  showPopUpTrue = false;

  @Input() showPopUp;
  @Output() closePopup = new EventEmitter();

  constructor() { }

  //added in all the components to close the popup
  closePopUp() {
    this.closePopup.emit();
  }

}