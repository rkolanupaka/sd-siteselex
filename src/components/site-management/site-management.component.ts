import { Component, OnInit, ViewChild } from "@angular/core";
import { CookieService } from "ngx-cookie";
import { DataService } from "../../common/services/data.service";
import { FormatSitesResponse } from "../../common/services/format-sites-response.service";

import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';

@Component({
  selector: "app-site-management",
  templateUrl: "./site-management.component.html",
  styleUrls: ["./site-management.component.scss"]
})
export class SiteManagementComponent implements OnInit {
  addSiteFlag: boolean = true;
  citylist = [];
  siteList = [];
  cityValue: any = "";
  cityValueValid: boolean = false;
  fiveBarsToken: string;
  parseInt = parseInt;
  showLoader: boolean = false;
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  siteAddUpdateStatus = false;
  selectedCity: string = "";
  statelist = [];
  stateVal: any = "";
  showSites = false;
  searchValue = "";
  searchValueFilter = "";
  searchByType = "Site ID";
  cityStatus = "enable";
  showAddSiteForm = false;
  siteFormModel: any;
  editedSiteTrack: any;
  statusFilter: string = '';
  statusFilterSites: string = '';
  currentPage = 1;
  initialEmailState:any = {};

  filteredCityList: Observable<any[]>;
  cityControl = new FormControl();

  @ViewChild("siteIdInForm") siteIdInForm;

  constructor(
    private dataservice: DataService,
    private _cookieService: CookieService,
    private formatSitesResponse: FormatSitesResponse
  ) {
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    this.resetEditSiteForm();
    this.cityControl.disable();
  }

  ngOnInit() {
    let url = "states";
    let headerDict = {
      "Content-Type": "application/json"
    };
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDict).subscribe(  //gets state list on page load
      (data:any) => {
        this.statelist = data.items;
        this.showLoader = false;
      },
      err => {
        this.showLoader = false;
        this.showPopUpMessage = "something went wrong. Please try again";
        this.showPopUpTrue = true;
      }
    );

    // sets city list for typeahead feature
    this.filteredCityList = this.cityControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value!=null ? this._filterCity(value): this.citylist.slice())
      );
  }

  typeaheadListFormat(data: any): string {
    return data.city;
  }

  typeaheadValueFormat(data: any): string {
    return data.city;
  }

  // filters cities on entering inside city typeahead
  private _filterCity(value: string): any {
    const filterValue = value.toLowerCase();
    this.cityValue = filterValue;
    return this.citylist.filter(option => option.city.toLowerCase().includes(filterValue));
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  // gets called on every state changes and updates the city list below it
  stateChanged(event) {
    console.log(this.stateVal);
    this.cityValue = "";
    this.cityControl.setValue('');
    if (this.stateVal != "") {
      this.showLoader = true;
      let url = "cities/" + this.stateVal.state_code;
      let headerDict = {
        "Content-Type": "application/json"
      };
      this.dataservice.fetchData(url, headerDict).subscribe((data:any) => {
        let citylistTemp = data.items;
        citylistTemp.forEach(element => {
          if (element.coming_soon == "1") {
            element.cityStatus = "comingSoon";
          } else if (element.enabled == "1") {
            element.cityStatus = "enable";
          } else {
            element.cityStatus = "disable";
          }
        });
        this.citylist = citylistTemp;
        if(this.citylist.length > 0) {
          this.cityControl.enable();
        }
        this.showLoader = false;
      });
    } else {
      this.citylist = [];
    }
  }

  // admin can change the status of the cities and also update city admin using this function
  cityStatusChanged(city, type?) {
    if(type === 'email' && city.admin_emailID) {
      if(city.admin_emailID === this.initialEmailState[city.city]) {
        city.editEmail = false;
        return false;
      }
    }
    city.cityStatusChangeSuccess = false;
    if(!type && city.editEmail && this.initialEmailState[city.city] !== undefined) {
      city.admin_emailID = this.initialEmailState[city.city];
    }
    city.editEmail = false;
    setTimeout(() => {
      if (city.cityStatus == "enable") {
        city.enabled = "1";
        city.coming_soon = "0";
      } else if (city.cityStatus == "disable") {
        city.enabled = "0";
        city.coming_soon = "0";
      } else {
        city.enabled = "0";
        city.coming_soon = "1";
      }
      let url = "cities";
      let headerDict = {
        "Content-Type": "application/json"
      };
      let cityData = {
        city_id: city.city_id,
        enabled: city.enabled,
        coming_soon: city.coming_soon,
        email: city.admin_emailID ? city.admin_emailID:""
      };
      city.cityStatusLoaderSuccess = true;
      this.dataservice.fetchPutData(url, cityData, headerDict).subscribe(
        data => {
          city.cityStatusChangeSuccess = true;
          city.cityStatusLoaderSuccess = false;
        },
        err => {
          city.cityStatusLoaderSuccess = false;
          this.showPopUpMessage = "something went wrong. Please try again";
          this.showPopUpTrue = true;
        }
      );
    }, 100);
  }

  // add/edit city admin
  editEmailSaveInitial(city) {
    this.initialEmailState[city.city] = city.admin_emailID;
    console.log(this.initialEmailState);
    city.editEmail = true;
  }

  // cancel add/edit city admin
  cancelEditEmail(city) {
    if(this.initialEmailState[city.city] !== undefined) {
      city.admin_emailID = this.initialEmailState[city.city];
    }
    city.editEmail = false;
  }

  // check if enetered city admin is valid
  checkEmailValid(email) {
    if(email){
      const re = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/;
      return re.test(email);
    }
    return false;
  }

  // get sites available for a city on clicking of view sites link next to city
  getSites(city) {
    console.log("state", this.stateVal.state);
    console.log("city", city.city);
    const url = "report/site/location/mapView/" + this.stateVal.state + "/" + city.city;
    const headerDict = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDict).subscribe(
      (data:any) => {
        this.siteList = data=='false'?[]:data;
        this.cityValue = city.city;
        this.cityControl.setValue(city.city);
        this.showSites = true;
        this.cityControl.disable();
        this.selectedCity = city;
        this.showLoader = false;
        this.currentPage = 1;
      },
      err => {
        this.showLoader = false;
        console.log(err);
      }
    );
  }

  // takes back from site view to city view
  closeSites() {
    this.showSites=false; 
    this.cityControl.enable();
    this.cityControl.setValue('');
    this.selectedCity = ''; 
    this.cityValue='';
    this.currentPage = 1;
  }

  // to filter sites based on filter
  searchValuKeyUp() {
    setTimeout(() => {
      if (this.searchValue == "") {
        this.searchValueFilter = "";
      }
    }, 200);
  }

  // opens the form with prefilled data on click of edit site
  editSiteFunc(site) {
    this.editedSiteTrack = site;
    this.siteAddUpdateStatus = false;
    console.log(site);
    this.siteFormModel = {
      siteId: site.sku,
      latitude: site.lat,
      longitude: site.long,
      locType: site.lType === "Downtown" ? site.lType : "Other",
      poleType: site.pCls,
      siteSelectionFee: site.ssFee,
      rentLeaseMonthlyFee: site.rlFee,
      siteSurveyLeaseFee: site.sslFee
    };
    this.addSiteFlag = false;
    this.showAddSiteForm = true;
    window.scroll({
      top: 200,
      behavior: "smooth"
    });
  }

  // resets add/edit site form to default values
  resetEditSiteForm() {
    this.siteFormModel = {
      siteId: "system generated",
      latitude: "",
      longitude: "",
      locType: "",
      poleType: "",
      siteSelectionFee: 750,
      rentLeaseMonthlyFee: 150,
      siteSurveyLeaseFee: 3000
    };
  }

  // focuses on first element in add/edit site form when form is opened
  focusFirstEl() {
    setTimeout(() => {
      this.siteIdInForm.nativeElement.focus();
    }, 1);
  }

  // common function to add new site or edit an existing one
  addUpdateSite() {
    let url = "";
    if (this.addSiteFlag) {   //if new site
      url = "sitemanagement/site";
    } else {   // if existing site
      url = "sitemanagement/site/" + this.siteFormModel.siteId;
    }
    let data: any = {
      product: {
        custom_attributes: [
          {
            attribute_code: "latitude",
            value: this.siteFormModel.latitude
          },
          {
            attribute_code: "state",
            value: this.stateVal.state
          },
          {
            attribute_code: "city",
            value: this.cityValue
          },
          {
            attribute_code: "longitude",
            value: this.siteFormModel.longitude
          },
          {
            attribute_code: "pole_class",
            value: this.siteFormModel.poleType
          },
          {
            attribute_code: "rent_lease_fee",
            value: this.siteFormModel.rentLeaseMonthlyFee
          },
          {
            attribute_code: "site_selection_fee",
            value: this.siteFormModel.siteSelectionFee
          },
          {
            attribute_code: "site_survey_lease_fee",
            value: this.siteFormModel.siteSurveyLeaseFee
          },
          {
            attribute_code: "city_area",
            value: this.siteFormModel.locType
          },
          {
            attribute_code: "electrical_service_field",
            value: "210v"
          }
        ]
      }
    };

    if (this.addSiteFlag) {
      data.product.custom_attributes.push({
        attribute_code: "reserved",
        value: "0"
      });
    } else {
      data.product.sku = this.siteFormModel.siteId;
      data.product.name = this.siteFormModel.siteId;
    }

    const headerDict = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.fiveBarsToken
    };
    
    this.showLoader = true;
    if (this.addSiteFlag) {
      this.dataservice.fetchPostData(url, data, headerDict).subscribe(
        data => {
          this.showLoader = false;
          this.addSiteValuePush(data);
        },
        err => {
          this.showLoader = false;
          if (err.message && err.message != "") {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please try again";
          }
          this.showPopUpTrue = true;
        }
      );
    } else {
      this.dataservice.fetchPutData(url, data, headerDict).subscribe(
        data => {
          this.showLoader = false;
          this.updateEditedSiteValue();
        },
        err => {
          this.showLoader = false;
          if (err.message && err.message != "") {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please try again";
          }
          this.showPopUpTrue = true;
        }
      );
    }
  }

  // shows the updated values of editied site in the below list
  updateEditedSiteValue() {
    this.editedSiteTrack.sku = this.siteFormModel.siteId;
    this.editedSiteTrack.lat = this.siteFormModel.latitude;
    this.editedSiteTrack.long = this.siteFormModel.longitude;
    this.editedSiteTrack.lType = this.siteFormModel.locType;
    this.editedSiteTrack.pCls = this.siteFormModel.poleType;
    this.editedSiteTrack.ssFee = this.siteFormModel.siteSelectionFee;
    this.editedSiteTrack.rlFee = this.siteFormModel.rentLeaseMonthlyFee;
    this.editedSiteTrack.sslFee = this.siteFormModel.siteSurveyLeaseFee;
    this.showAddSiteForm = false;
    this.resetEditSiteForm();
    this.siteAddUpdateStatus = true;
  }

  // pushes new added site to below list
  addSiteValuePush(data) {
    this.siteList.push({
      sku: data.sku,
      lat: this.siteFormModel.latitude,
      long: this.siteFormModel.longitude,
      stat: "available",
      lType: this.siteFormModel.locType,
      pCls: this.siteFormModel.poleType,
      ssFee: this.siteFormModel.siteSelectionFee,
      rlFee: this.siteFormModel.rentLeaseMonthlyFee,
      sslFee: this.siteFormModel.siteSurveyLeaseFee
    });
    this.showAddSiteForm = false;
    this.resetEditSiteForm();
    this.siteAddUpdateStatus = true;
  }

  // to update the status of existing site i.e. avaialble/unavailable/reserved
  editSiteStatus(event, site) {
    site.siteEditSuccess = false;
    let url = "sitemanagement/site/" + site.sku;
    const headerDict = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + this.fiveBarsToken
    };

    let data: any = {
      product: {
        sku: site.sku,
        name: site.sku,
        custom_attributes: []
      }
    };

    if (event.target.value == "reserved") {
      data.product.custom_attributes.push({
        attribute_code: "reserved",
        value: event.target.checked ? 1 : 0
      });
    } else {
      data.product.status = event.target.checked ? 1 : 2;
    }
    site.siteEditLoaderSuccess = true;
    this.dataservice.fetchPutData(url, data, headerDict).subscribe(
      data => {
        site.siteEditLoaderSuccess = false;
        site.siteEditSuccess = true;
      },
      err => {
        event.target.checked = !event.target.checked;
        site.siteEditLoaderSuccess = false;
        site.siteEditSuccess = true;
        if (err.message && err.message != "") {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "something went wrong. Please try again";
        }
        this.showPopUpTrue = true;
      }
    );
  }

}
