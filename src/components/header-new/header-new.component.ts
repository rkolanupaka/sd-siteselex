import {
  Component,
  OnInit,
  Input,
  Output,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  EventEmitter,
  HostListener
} from "@angular/core";
import { Router } from "@angular/router";

import { CookieService } from "ngx-cookie";
import { DataService } from "../../common/services/data.service";

@Component({
  selector: "app-header-new",
  templateUrl: "./header-new.component.html",
  styleUrls: ["./header-new.component.scss"]
})
export class HeaderNewComponent implements OnInit {
  @Output() loggedUserDetails = new EventEmitter<any>();
  @Input() sitesZeroCartCount: number;

  customerId: string;
  cartAvailable: boolean = false;
  fiveBarsToken: string;
  idealTime: number = 0;
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  showLoader: boolean = false;
  userName: string;
  validUserForReports: boolean = false;
  domainUser: string = "";

  private timer: any;

  openNav: boolean = false;

  constructor(private dataservice: DataService, private _cookieService: CookieService, private router: Router) {
    // starts timer to track user inactivity and auto logout after 15 mints
    this.timer = setInterval(() => {
      this.timerIncrement();
    }, 1000 * 60);
  }

  //clears the times on component destroy
  ngOnDestroy() {
    clearInterval(this.timer);
  }

  // update header cart icon functionality i.e. cart only works if cart in non-empty else it is disabled
  ngOnChanges(changes: SimpleChanges) {
    this.cartAvailable = false;
  }

  // closes custom alert popup
  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  // makes api call to get customer details on load
  ngOnInit() {
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    let userDetails = localStorage.getItem("fivebarsUserData");

    if (userDetails == undefined) { //if user details already presents then doesn't make api call
      let url = "customers/me";
      let headerDect = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      this.dataservice.fetchData(url, headerDect).subscribe(
        (data: any) => {

          let securityQueCheck = false; // checks for new users if they have completed security question in their profile. if not navigates them to that page.
          if(data.custom_attributes) {
            (data.custom_attributes).forEach((element) => {
              if(element.attribute_code == 'security_info_updated') {
                if(element.value == "1") {
                  securityQueCheck = true;
                }
              }
            });
          }

          if(data.addresses.length > 0) {  // checks for new users if they have saved address in their profile. if not navigates them to that page.
            localStorage.setItem("fivebarsUserData", JSON.stringify(data));
          }
          
          if (data.addresses.length > 0 && securityQueCheck) {
            this.getUserDetailsSuccess(data);
          } else {
            this.getUserDetailsSuccess(data);
            this.router.navigate(["profile"]);
          }
        },
        err => {
          if (err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please login again";
          }
          this.showPopUpTrue = true;
          setTimeout(() => {
            this.router.navigate([""]);
          }, 4000);
        }
      );
    } else {
      this.getUserDetailsSuccess(JSON.parse(localStorage.getItem("fivebarsUserData")));
    }

    if (     // enables the header cart icon if cart items are present
      localStorage.getItem("selectedSiteDataWidCart") &&
      JSON.parse(localStorage.getItem("selectedSiteDataWidCart")).length > 0
    ) {
      this.cartAvailable = true;
    }
  }

  getUserDetailsSuccess(data) {
    this.customerId = data.id;
    this.loggedUserDetails.emit(data);   //emits user details from header component so that some other components can access this info
    if(data.custom_attributes) {
      (data.custom_attributes).forEach((element) => {
        if(element.attribute_code == 'is_admin') {   // if user is an admin shows admin links like site management, reports in header nav menu
          if(element.value == "1") {
            this.validUserForReports = true;
          }
        }
      });
    }
    this.userName = data.firstname.substr(0, 1) + data.lastname.substr(0, 1);
    let emailSplitArray = data.email.split("@");
    emailSplitArray = emailSplitArray[emailSplitArray.length - 1].toLowerCase();
    this.domainUser = emailSplitArray.substring(0, emailSplitArray.indexOf("."));
  }

  // host listners to track if user is active or not. if now we will logout after 15 mints
  @HostListener("document:mousemove", ["$event"])
  mousemove(event) {
    this.idealTime = 0;
  }

  @HostListener("document:keydown", ["$event"])
  keydown(event) {
    this.idealTime = 0;
  }

  timerIncrement() {
    // logout user after 15 minutes of inactivity
    this.idealTime += 1;
    if (this.idealTime > 14) {
      this.showPopUpMessage = "Your session has expired. Please login again.";
      this.showPopUpTrue = true;
      setTimeout(() => {
        this.logout();
      }, 3000);
    }
  }

  logout() {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    let url = "customer/revoke/" + this.customerId;
    this.showLoader = true;
    this.dataservice.fetchPostData(url, headerDict).subscribe(
      data => {
        this.showLoader = false;
        localStorage.clear();
        this._cookieService.removeAll();
        this.router.navigate([""]);
      },
      err => {
        this.showLoader = false;
        localStorage.clear();
        this._cookieService.removeAll();
        this.router.navigate([""]);
      }
    );
  }

  navigateToPayment() {
    if (this.cartAvailable) {
      this.router.navigate(["site/payment"]);
    }
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }
}
