import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthenticationGuard } from '../common/guards/authentication.guard';
import { AdminGuard } from '../common/guards/admin.guard';

import { LoginComponent } from '../components/login/login.component';
import { ForgotpasswordComponent } from "../components/forgotpassword/forgotpassword.component";

const routes: Routes = [
    {
        path: 'site',
        loadChildren: '../modules/purchase/purchase.module#PurchaseModule',
        canActivate: [AuthenticationGuard]    //added a guard so that only after valid login user can visit this URL
    },

    {
        path: 'profile',
        loadChildren: '../modules/profile/profile.module#ProfileModule',
        canActivate: [AuthenticationGuard]    //added a guard so that only after valid login user can visit this URL
    },

    {
        path: 'accountmanagement',
        loadChildren: '../modules/account-management/account-management.module#AccountManagementModule',
        canActivate: [AuthenticationGuard]    //added a guard so that only after valid login user can visit this URL
    },

    {
        path: 'orderhistory',
        loadChildren: '../modules/orders/orders.module#OrdersModule',
        canActivate: [AuthenticationGuard]    //added a guard so that only after valid login user can visit this URL
    },

    {
        path: 'admin',
        loadChildren: '../modules/admin/admin.module#AdminModule',
        canActivate: [AuthenticationGuard, AdminGuard]    //added a guard so that only admin users and only after valid login user can visit this URL
    },

    {
        path: 'resetpassword/:id/:token',
        loadChildren: '../modules/reset-password/reset-password.module#ResetPasswordModule'
    },

    {
        path: 'contactus',
        loadChildren: '../modules/contact-us/contact-us.module#ContactUsModule',
        canActivate: [AuthenticationGuard]   //added a guard so that only after valid login user can visit this URL
    },

    {
        path: 'comingsoon',
        loadChildren: '../modules/coming-soon/coming-soon.module#ComingSoonModule',
        canActivate: [AuthenticationGuard]   //added a guard so that only after valid login user can visit this URL
    },

    {
        path: 'requestsite',
        loadChildren: '../modules/request-site/request-site.module#RequestSiteModule',
        canActivate: [AuthenticationGuard]    //added a guard so that only after valid login user can visit this URL 
    },

    {
        path: 'forgotpassword',
        component: ForgotpasswordComponent
    },

    {
        path: 'login',
        component: LoginComponent
    },

    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },

    {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
})

export class AppRoutingModule { }
